﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BubbleSort
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private async void Button_Click(object sender, RoutedEventArgs e)
    {
      int[] arr = new int[50000];

      for (int i = 49999; i > 0; i--)
        arr[i] = i;

      //use this for responsivness - even though we create a new task/thread, we don't need dispatcher.invoke
      //var sortedArray = await Task.Run(() => Sort(arr)); 
      var sortedArray = await Sort(arr);

      LogTextBlock.Text += string.Join(", ", sortedArray);
    }

    // although this function is marked as async it will not be responsive automatically
    private async Task<int[]> Sort(int[] arr)
    {
      int temp = 0;
      for (int write = 0; write < arr.Length; write++)
      {
        for (int sort = 0; sort < arr.Length - 1; sort++)
        {
          await Task.Yield();
          if (arr[sort] > arr[sort + 1])
          {
            temp = arr[sort + 1];
            arr[sort + 1] = arr[sort];
            arr[sort] = temp;
          }
        }
      }
      return arr;
    }
  }
}
