﻿using System;
using System.Windows;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BreakfastMakerParallel
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      LogTextBlock.Text += $"--I'm hungry, let's make breakfast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";

      var toast = MakeToast();
      var coffee = MakeCoffee();

      LogTextBlock.Text += "xxx";
      Task.WhenAll(toast, coffee).ContinueWith((t) =>
      {
        var text = $"--Done, let's eat! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
        Dispatcher.Invoke(() =>
        {
          LogTextBlock.Text += text;
        });
      });
    }

    private Task MakeToast()
    {
      return Task.Run(() =>
      {
        var text = $"Making toast in the toaster! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
        Dispatcher.Invoke(() => { LogTextBlock.Text += text; });
        Thread.Sleep(3000);
        text = $"Putting butter on toast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
        Dispatcher.Invoke(() => { LogTextBlock.Text += text; });
        Thread.Sleep(500);
      });
    }

    private Task MakeCoffee()
    {
      return Task.Run(() =>
      {
        var text = $"Boiling water! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
        Dispatcher.Invoke(() => { LogTextBlock.Text += text; });
        Thread.Sleep(3000);
        text = $"Pouring water into cup! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
        Dispatcher.Invoke(() => { LogTextBlock.Text += text; });
        Thread.Sleep(500);
      });
    }
  }
}
