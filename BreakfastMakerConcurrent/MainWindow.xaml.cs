﻿using System;
using System.Windows;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BreakfastMakerConcurrent
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private async void Button_Click(object sender, RoutedEventArgs e)
    {
      LogTextBlock.Text += $"--I'm hungry, let's make breakfast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";

      var toast = MakeToast();
      var coffee = MakeCoffee();

      await Task.WhenAll(toast, coffee);

      LogTextBlock.Text += $"--Done, let's eat! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
    }

    private async Task MakeToast()
    {
      LogTextBlock.Text += $"Making toast in the toaster! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      await Task.Delay(3000); // try: await Task.Yield();

      LogTextBlock.Text += $"Putting butter on toast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      await Task.Delay(500);
    }

    private async Task MakeCoffee()
    {
      LogTextBlock.Text += $"Boiling water! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      await Task.Delay(3000);

      LogTextBlock.Text += $"Pouring water into cup! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      await Task.Delay(500);
    }
  }
}
