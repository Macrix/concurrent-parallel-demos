﻿using System;
using System.Windows;
using System.Threading;
using System.Diagnostics;

namespace BreakfastMaker
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      LogTextBlock.Text += $"--I'm hungry, let's make breakfast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";

      MakeToast();
      MakeCoffee();

      LogTextBlock.Text += $"--Done, let's eat! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
    }

    private void MakeToast()
    {
      LogTextBlock.Text += $"Making toast in the toaster! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      Thread.Sleep(3000);

      LogTextBlock.Text += $"Putting butter on toast! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      Thread.Sleep(500);
    }

    private void MakeCoffee()
    {
      LogTextBlock.Text += $"Boiling water! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      Thread.Sleep(3000);

      LogTextBlock.Text += $"Pouring water into cup! I'm person {Thread.CurrentThread.ManagedThreadId}\n";
      Thread.Sleep(500);
    }
  }
}
